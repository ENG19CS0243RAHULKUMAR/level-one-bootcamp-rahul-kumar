//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float distance_calc(float m1, float m2, float n1, float n2)
{
  float distance;
  distance = sqrt( (m1 - n1)*(m1 - n1)+(m2 - n2)*(m2 - n2) );
  return distance;
}
int main()
{
  float ans,p,q,r,s;
    printf("\n Enter The Coordinates of Point P:\n");
    printf("\n M - Axis Coordinate: \t");
    scanf("%f", &p);
    printf("\n N - Axis Coordinate: \t");
    scanf("%f", &q);    
    printf("\n Enter The Coordinates of Point Q:\n");
    printf("\n N - Axis Coordinate:\t");
    scanf("%f", &r);
    printf("\n N - Axis Coordinate: \t");
    scanf("%f", &s);
    ans = distance_calc(p, q, r, s);
    printf("\n Distance between Points P and Q: %f\n", ans);
    return 0;
}