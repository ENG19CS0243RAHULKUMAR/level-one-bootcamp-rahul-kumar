//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>

struct Points 
{
    float x, y;
}
 
float distance_calculator(struct Points r, struct Points k)
{
    float result;
    result = sqrt((r.x - k.x) * (r.x - k.x) + (r.y - k.y) *(r.y - k.y));
    return result;
}
 
int main()
{
    struct Points r, struct Points k;
    printf("\nEnter The Coordinates of Point M:\n");
    printf("\nX - Axis Coordinate: \t");
    scanf("%f", &r.x);
    printf("\nY - Axis Coordinate: \t");
    scanf("%f", &r.y);  
    printf("\nEnter The Coordinates of Point N:\n");
    printf("\nY - Axis Coordinate:\t");
    scanf("%f", &k.x);
    printf("\nY - Axis Coordinate: \t");
    scanf("%f", &k.y);
    printf("\nDistance between Points M and N: %f\n", distance_calculator(r, k));
    return 0;
}