//WAP to find the sum of n fractions.
#include <stdio.h>
#include <bits/stdc++.h>
using namespace std;
 
int gcd(int a, int b)
{
    if (b == 0) {
        return a;
    }
 
    return gcd(b, a % b);
}
 
int findlcm(int arr[], int n)
{
   
    int ans = arr[0];
 
   
    for (int i = 1; i < n; i++) {
        ans = (((arr[i] * ans)) / (gcd(arr[i], ans)));
    }
 
    return ans;
}
 
void addReduce(int n, int num[],
               int den[])
{
 
   
    int final_numerator = 0;
 
    int final_denominator = findlcm(den, n);
 
    for (int i = 0; i < n; i++) {
 
        final_numerator = final_numerator
                          + (num[i]) * (final_denominator
                                        / den[i]);
    }
 
    int GCD = gcd(final_numerator,
                  final_denominator);
 
    
    final_numerator /= GCD;
    final_denominator /= GCD;
 
    cout << final_numerator
         << "/"
         << final_denominator
         << endl;
}
 
int main()
{
    int N = 3;
 
    int arr1[] = { 1, 2, 5 };
 
    int arr2[] = { 2, 1, 6 };
 
    addReduce(N, arr1, arr2);
    return 0;
}