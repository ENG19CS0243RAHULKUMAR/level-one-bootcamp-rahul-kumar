//WAP to find the sum of two fractions.
#include <stdio.h>
int sum(int a, int b) {
   if (a == 0)
      return b;
   return sum(b%a, a);
}
void smallest(int &deno3, int &num3) 
{
   int common_factor = sum(num3,deno3);
   deno3 = deno3/common_factor;
   num = num3/common_factor;
}
void add_fraction(int num1, int deno1, int num2, int deno2, int &num3, int &deno3) 
{
   deno3 = sum(deno1,deno2);
   deno3 = (deno1*deno2) / deno3;
   num3 = (num1)*(deno3/deno1) + (num2)*(deno3/deno2);
   smallest(deno3,num3);
}
int main() {
   int num1=1, deno1=4, num2=2, deno2=12, deno3, num3;
   add_fraction(num1, deno1, num2, deno2, num3, deno3);
   printf("%d/%d + %d/%d = %d/%d\n", num1, deno1, num2, deno2, num3, deno3);
   return 0;
}