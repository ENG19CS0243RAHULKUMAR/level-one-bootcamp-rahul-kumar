//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>

int input()
{
   int n;
   printf("Up to which number you want to find the sum: ");
   scanf("%d", &n);
   return n;
}
int sum(int num)
{
   int add = 0;
   for(int i=1; i<=num; i++)
   {
     add += i;
   }
   return add;
}

void display(int n, int sum)
{
   printf("1+2+3+….+%d+%d = %d",n-1, n, sum);
}

int main()
{
   int range, result;
   range = input();
   result = sum(range);
   display(range, result);
}
